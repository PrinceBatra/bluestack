import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Util {
  static Future movePage(BuildContext context, Function pageToMove,
      {bool replacement = false}) {
    if (replacement) {
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (BuildContext context) => pageToMove()));
    } else {
      Navigator.push(context,
          MaterialPageRoute(builder: (BuildContext context) => pageToMove()));
    }
  }

  static void showToast(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.blue,
        textColor: Colors.white,
        fontSize: 16.0);
  }
}
