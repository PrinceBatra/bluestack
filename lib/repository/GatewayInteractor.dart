import 'package:bluestack/repository/local/SharedPreferenceManager.dart';
import 'package:bluestack/repository/remote/NetworkManager.dart';
import 'package:http/http.dart';

class GatewayInteractor{

  Future<Response> getGames(String _cursor){
    Map<String, String> queryParam = NetworkManager.getDefaultQueryParam();
    if (_cursor != null && _cursor.length > 0) {
      queryParam['cursor'] = _cursor;
    }
    return NetworkManager.getResponse(NetworkManager.bluestackUrl, queryParam);
  }

  Future<Response> getUserDetails(String mobileNumber){
    return NetworkManager.getResponse(
        NetworkManager.userDetailUrl, {'mobile': mobileNumber});
  }

  Future<String> getMobileNumber() async {
    return await SharedPreferenceManager().getString(SharedPreferenceManager.MOBILE_NUMBER);
  }

  Future<bool> setMobileNumber(String mobile){
    return SharedPreferenceManager().setString(SharedPreferenceManager.MOBILE_NUMBER, mobile);
  }

}