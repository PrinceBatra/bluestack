class TranslationManager{

  static final TranslationManager _singleton = TranslationManager._internal();


  factory TranslationManager() {
    return _singleton;
  }

  TranslationManager._internal();



  String getSignIn(){
      return 'Sign In';
  }


  String getPhoneNumber(){
    return 'Phone Number';
  }

  String getPassword(){
    return 'Password';
  }

  String getForgotPassword(){
    return 'Forgot Password';
  }

  String getLogin(){
    return 'Login';
  }

  String getDontHaveAccount(){
    return 'Does not have account?';
  }

  String getSignUp(){
    return 'Sign Up';
  }

  String getEmptyError(){
    return 'Can\'t be empty';
  }

  String getPhoneNumberPasswordLengthError(){
    return 'Should be minimum 3 characters long';
  }

  String failedToGetResponse(){
    return 'Failed to get response.';
  }

  String retry(){
    return 'Retry';
  }

  String tournamentPlayed(){
    return 'Tournament\nplayed';
  }

  String tournamentWon(){
    return 'Tournament\nwon';
  }

  String tournamentPercentage(){
    return 'Winning\npercentage';
  }

  String recommended(){
    return 'Recommended for you';
  }

  String invalidePhoneNumber(){
    return 'Invalid Phone Number';
  }

  String invalidPassword(){
    return 'Invalid Password';
  }



}