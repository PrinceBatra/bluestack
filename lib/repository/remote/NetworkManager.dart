import 'package:http/http.dart' as http;

class NetworkManager {
  static String bluestackUrl = 'http://tournaments-dot-game-tv-prod.uc.r.appspot.com/tournament/api/tournaments_list_v2';
  static String userDetailUrl = 'http://77.137.10.170:31126/bluestack/bluestackuserdetails';

  static Map<String, String> getDefaultQueryParam() {
    return {'limit': '10', 'status': 'all'};
  }

  static Future<http.Response> getResponse(String url, Map<String, String> queryParam) async {
    var uri = Uri.parse(url).replace(queryParameters: queryParam);
    var response = await http.get(uri);
    if (response.statusCode != 200 ||
        response.body == null ||
        response.body.length <= 0) return null;
    return response;
  }
}
