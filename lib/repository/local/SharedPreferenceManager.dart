import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceManager{

  static final String MOBILE_NUMBER = 'mobile_number';

  static final SharedPreferenceManager _singleton = SharedPreferenceManager._internal();


  factory SharedPreferenceManager() {
    return _singleton;
  }

  SharedPreferenceManager._internal();


  Future<String> getString(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.getString(key);
  }

  Future<bool> setString(String key, String value) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setString(key, value);
  }


}