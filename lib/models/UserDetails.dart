class UserDetails {
  String name;
  int tournamentPlayed;
  int tournamentWon;
  int winningPercentage;
  String rating;
  String imageUrl;

  UserDetails(
      {this.name,
        this.tournamentPlayed,
        this.tournamentWon,
        this.winningPercentage,
        this.rating,
        this.imageUrl});

  UserDetails.fromJson(Map<String, dynamic> json) {
    name = json['Name'];
    tournamentPlayed = json['TournamentPlayed'];
    tournamentWon = json['TournamentWon'];
    winningPercentage = json['WinningPercentage'];
    rating = json['Rating'];
    imageUrl = json['ImageUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Name'] = this.name;
    data['TournamentPlayed'] = this.tournamentPlayed;
    data['TournamentWon'] = this.tournamentWon;
    data['WinningPercentage'] = this.winningPercentage;
    data['Rating'] = this.rating;
    data['ImageUrl'] = this.imageUrl;
    return data;
  }
}