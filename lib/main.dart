import 'package:bluestack/repository/GatewayInteractor.dart';
import 'package:bluestack/screens/HomeScreen.dart';
import 'package:bluestack/screens/LoginScreen.dart';
import 'package:bluestack/utils/Util.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Bluestack',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Splash(),
    );
  }
}

class Splash extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashState();
  }
}

class SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    launchFirstScreen();
  }

  void launchFirstScreen() async {
    GatewayInteractor().getMobileNumber().then((value) {
      if (value != null && value.length > 0) {
        Util.movePage(context, () {
          return HomeScreen();
        }, replacement: true);
      } else {
        Util.movePage(context, () {
          return LoginScreen();
        }, replacement: true);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(),
    );
  }
}
