import 'dart:convert';

import 'package:bluestack/Observer.dart';
import 'package:bluestack/repository/remote/TranslationManager.dart';
import 'package:bluestack/screenControllers/UserDetailsController.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class UserDetailsWidget extends StatefulWidget {
  UserDetailsController _userDetailsController;
  UserDetailsWidget(this._userDetailsController);

  @override
  State<StatefulWidget> createState() {
    return _UserDetailsWidgetState();
  }
}

class _UserDetailsWidgetState extends State<UserDetailsWidget> implements Observer{


  @override
  void notify() {
    setState(() {
    });
  }

  @override
  void initState() {
    super.initState();
    widget._userDetailsController.setObserver(this);
    if (widget._userDetailsController.userDetails == null) {
      widget._userDetailsController.getUserDetails();
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget._userDetailsController.isLoading()) {
      return _getLoadingWidget();
    } else if (widget._userDetailsController.isError()) {
      return _getErrorWidget();
    }
    if (widget._userDetailsController.userDetails == null) return SizedBox();

    return _getSuccessWidget();
  }

  Widget _getLoadingWidget() {
    return SizedBox();
  }

  Widget _getErrorWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(
          Icons.error,
          color: Colors.grey,
          size: 22,
        ),
        SizedBox(
          height: 20,
        ),
        FlatButton(
          onPressed: () {
            widget._userDetailsController.getUserDetails();
          },
          textColor: Colors.blue,
          child: Text(TranslationManager().retry()),
        ),
      ],
    );
  }

  Widget _getSuccessWidget() {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 20, 20, 10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _getNameImage(),
          SizedBox(height: 10),
          _getDashboardChip(),
          _getRecommend(),
        ],
      ),
    );
  }

  Widget _getRecommend() {
    return Text(
      TranslationManager().recommended(),
      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
    );
  }

  Widget _getSingleItem(String count, String text, double left, double right,
      Color colorOne, Color colorTwo) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [colorOne, colorTwo],
              begin: const FractionalOffset(0.0, 0.0),
              end: const FractionalOffset(1.0, 1.0),
              stops: [0.0, 1.0],
              tileMode: TileMode.clamp),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(left),
              bottomLeft: Radius.circular(left),
              topRight: Radius.circular(right),
              bottomRight: Radius.circular(right))),
      child: Text(
        count + '\n' + text,
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold,fontSize: 14),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _getDashboardChip() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _getSingleItem(
            widget._userDetailsController.userDetails.tournamentPlayed
                .toString(),
            TranslationManager().tournamentPlayed(),
            20,
            0,
            Color(0xffdb6208),
            Color(0xffe69808)),
        _getSingleItem(
            widget._userDetailsController.userDetails.tournamentWon.toString(),
            TranslationManager().tournamentWon(),
            0,
            0,
            Color(0xff310e83),
            Color(0xff903aae)),
        _getSingleItem(
            widget._userDetailsController.userDetails.winningPercentage
                .toString(),
            TranslationManager().tournamentPercentage(),
            0,
            20,
            Color(0xffe53b35),
            Color(0xffe9673e)),
      ],
    );
  }

  Widget _getNameImage() {
    return Row(
      children: [
        _getProfileImage(),
        SizedBox(width: 15),
        Expanded(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _getName(),
            _getRating(),
          ],
        ))
      ],
    );
  }

  Widget _getName() {
    return Text(
      widget._userDetailsController.userDetails.name,
      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
    );
  }

  Widget _getRating() {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
      padding: EdgeInsets.fromLTRB(10, 8, 20, 8),
      decoration: BoxDecoration(
          border: Border.all(
            color: Colors.blue,
          ),
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(20))),
      child: Text(
        widget._userDetailsController.userDetails.rating,
        style: TextStyle(color: Colors.blue),
      ),
    );
  }

  Widget _getProfileImage() {
    return Container(
      height: 80,
      width: 80,
      child: CachedNetworkImage(
        imageUrl: widget._userDetailsController.userDetails.imageUrl,
        imageBuilder: (context, imageProvider) => Container(
          decoration: new BoxDecoration(
              image: DecorationImage(
                image: CachedNetworkImageProvider(
                  widget._userDetailsController.userDetails.imageUrl,
                ),
                fit: BoxFit.cover,
              ),
              borderRadius: new BorderRadius.all(Radius.circular(1000))),
        ),
        placeholder: (context, url) => Icon(
          Icons.image,
          size: 50,
          color: Colors.grey,
        ),
        errorWidget: (context, url, error) => Icon(
          Icons.error,
          color: Colors.red,
          size: 80,
        ),
      ),
    );
    ;
  }
}

