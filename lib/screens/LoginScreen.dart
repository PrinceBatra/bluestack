import 'package:bluestack/repository/remote/TranslationManager.dart';
import 'package:bluestack/screenControllers/LoginScreenController.dart';
import 'package:bluestack/screens/HomeScreen.dart';
import 'package:bluestack/utils/ConstantUtil.dart';
import 'package:bluestack/utils/Util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginScreenState();
  }
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _formValidate = false;
  LoginScreenController _loginScreenController;

  @override
  void initState() {
    _loginScreenController = LoginScreenController(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(ConstantUtil.appName),
      ),
      body: Form(
        key: _formKey,
        onChanged: (){
          setState(() {
            _formValidate = _formKey.currentState.validate();
          });
        },
        child: Padding(
          padding: EdgeInsets.all(10),
          child: ListView(
            children: <Widget>[
              _getLogo(),
              _getSignInLabel(),
              _getPhoneNumberTextFiled(),
              _getPasswordField(),
              _getForgotPassword(),
              _getLoginButton(),
              _getSignUp()
            ],
          ),
        ),
      ),
    );
  }

  void _loginPressed(){
    if(_formKey.currentState.validate()){
      _loginScreenController.loginClicked(_phoneController.text, _passwordController.text);

    }
  }

  Widget _getSignUp() {
    return Container(
        child: Row(
      children: <Widget>[
        Text(TranslationManager().getDontHaveAccount()),
        FlatButton(
          textColor: Colors.blue,
          child: Text(
            TranslationManager().getSignUp(),
            style: TextStyle(fontSize: 20),
          ),
          onPressed: () {
            Util.showToast(ConstantUtil.Not_Implemented);
          },
        )
      ],
      mainAxisAlignment: MainAxisAlignment.center,
    ));
  }

  Widget _getLoginButton() {
    return Container(
        height: 50,
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: RaisedButton(
          textColor: Colors.white,
          color: _formValidate ? Colors.blue : Colors.blue[100],
          child: Text(TranslationManager().getLogin()),
          onPressed: _loginPressed,
        ));
  }

  Widget _getForgotPassword() {
    return FlatButton(
      onPressed: () {
        Util.showToast(ConstantUtil.Not_Implemented);
      },
      textColor: Colors.blue,
      child: Text(TranslationManager().getForgotPassword()),
    );
  }

  Widget _getPasswordField() {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
      child: TextFormField(
        validator: (value){
          if(value.isEmpty)
            return TranslationManager().getEmptyError();
          if(value.length <3)
            return TranslationManager().getPhoneNumberPasswordLengthError();
          else if(value != 'password123')
            return TranslationManager().invalidPassword();
          return null;
        },
        obscureText: true,
        maxLength: 11,
        controller: _passwordController,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          counterText: '',
          labelText: TranslationManager().getPassword(),
        ),
      ),
    );
  }

  Widget _getPhoneNumberTextFiled() {
    return Container(
      padding: EdgeInsets.all(10),
      child: TextFormField(
        validator: (value){
          if(value.isEmpty)
            return TranslationManager().getEmptyError();
          if(value.length <3)
            return TranslationManager().getPhoneNumberPasswordLengthError();
          else if(value != '9898989898' && value != '9876543210' && value != '8376903335')
            return TranslationManager().invalidePhoneNumber();
          return null;
        },
        controller: _phoneController,
        maxLength: 10,
        keyboardType: TextInputType.phone,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          counterText: '',
          labelText: TranslationManager().getPhoneNumber(),
        ),
      ),
    );
  }

  Widget _getSignInLabel() {
    return Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(10),
        child: Text(
          TranslationManager().getSignIn(),
          style: TextStyle(fontSize: 20),
        ));
  }

  Widget _getLogo() {
    return Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(10),
        child: Image.asset(
          ConstantUtil.gameTvLogo,
          width: double.infinity,
          height: 50,
        ));
  }
}
