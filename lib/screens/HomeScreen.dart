import 'dart:convert';

import 'package:bluestack/Observer.dart';
import 'package:bluestack/repository/remote/TranslationManager.dart';
import 'package:bluestack/models/Tournaments.dart';
import 'package:bluestack/screenControllers/HomeScreenController.dart';
import 'package:bluestack/screenControllers/UserDetailsController.dart';
import 'package:bluestack/utils/ConstantUtil.dart';
import 'package:bluestack/widgets/UserDetailsWidget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:shimmer/shimmer.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State<HomeScreen> implements Observer {
  HomeScreenController _homeScreenController;
  UserDetailsController _userDetailsController;

  @override
  void notify() {
    setState(() {});
  }

  @override
  void initState() {
    _homeScreenController = HomeScreenController(this,context);
    _userDetailsController = UserDetailsController();
    super.initState();
    _homeScreenController.getGames();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          ConstantUtil.appName,
          style: TextStyle(color: Colors.black),
        ),
        leading: Icon(
          Icons.menu,
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        actions: [
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
            child: GestureDetector(
              onTap: (){
                _homeScreenController.logout();
              },
              child: Icon(
                Icons.logout,
                color: Colors.black,
              ),
            ),
          )
        ],
      ),
      body: ListView.builder(
        itemCount: _homeScreenController.getTournamentList().length + 2,
        itemBuilder: (context, index) {
          _homeScreenController.loadMoreIfNeeded(index);

          if (index == _homeScreenController.getTournamentList().length + 1)
            return _showListLastWidget();
          else if (index == 0) return UserDetailsWidget(_userDetailsController);
          return ListTile(title: _getSingleGameWidget(index - 1));
        },
      ),
    );
  }

  Widget _getSingleGameWidget(int position) {
    return Card(
      margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25))),
      child: Column(
        children: <Widget>[
          _getImage(position),
          _getTitle(position),
        ],
      ),
    );
  }

  Widget _getTitle(int position) {
    Tournaments singleTour =
        _homeScreenController.getTournamentList()[position];
    return ListTile(
      title: Text(
        singleTour.gameName,
        style: TextStyle(
          fontWeight: FontWeight.bold,
        ),
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
      subtitle: Text(
        singleTour.gameFormat,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
      trailing: Icon(
        Icons.chevron_right,
      ),
    );
  }

  Widget _getImage(int position) {
    Tournaments singleGame =
        _homeScreenController.getTournamentList()[position];
    return Container(
      height: 100,
      child: CachedNetworkImage(
        imageUrl: singleGame.coverUrl,
        imageBuilder: (context, imageProvider) => Container(
          decoration: new BoxDecoration(
              image: DecorationImage(
                image: CachedNetworkImageProvider(
                  singleGame.coverUrl,
                ),
                fit: BoxFit.cover,
              ),
              borderRadius: new BorderRadius.only(
                  topRight: Radius.circular(25), topLeft: Radius.circular(25))),
        ),
        placeholder: (context, url) => _getLoadingShimmer(),
        errorWidget: (context, url, error) => Icon(
          Icons.error,
          color: Colors.red,
          size: 80,
        ),
      ),
    );
  }

  Widget _showListLastWidget() {
    if (_homeScreenController.isLoading())
      return _getLoadingShimmer();
    else if (_homeScreenController.isError()) return _getRetryWidget();
    return SizedBox();
  }

  Widget _getRetryWidget() {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
      color: Colors.grey[100],
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(TranslationManager().failedToGetResponse()),
          ),
          FlatButton(
            onPressed: () {
              _homeScreenController.getGames();
            },
            textColor: Colors.blue,
            child: Text(TranslationManager().retry()),
          ),
        ],
      ),
    );
  }

  Widget _getLoadingShimmer() {
    return Padding(
      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: Shimmer.fromColors(
        baseColor: Colors.grey[300],
        highlightColor: Colors.grey[100],
        child: Card(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(40))),
          child: Container(
            height: 160,
          ),
        ),
      ),
    );
  }
}
