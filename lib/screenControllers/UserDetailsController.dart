import 'dart:convert';

import 'package:bluestack/models/UserDetails.dart';
import 'package:bluestack/repository/GatewayInteractor.dart';
import 'package:http/http.dart';

import '../Observer.dart';

class UserDetailsController {
  UserDetails userDetails;
  bool _loading = false;
  bool _error = false;
  Observer observer;

  void setObserver(Observer obser) {
    this.observer = obser;
  }

  bool isLoading() {
    return _loading;
  }

  bool isError() {
    return _error;
  }

  void getUserDetails() {
    if (_loading) return;
    _showHideLoading(true);
    GatewayInteractor().getMobileNumber().then((value) {
      GatewayInteractor().getUserDetails(value).then((Response value) {
        if (value == null)
          _showHideError(true);
        else
          _showHideError(false);

        this.userDetails = UserDetails.fromJson(jsonDecode(value.body));
        _showHideLoading(false);
      });
    });


  }

  void _showHideError(bool showError) {
    _error = showError;
    observer.notify();
  }

  void _showHideLoading(bool load) {
    _loading = load;
    observer.notify();
  }
}
