import 'dart:convert';

import 'package:bluestack/Observer.dart';
import 'package:bluestack/repository/GatewayInteractor.dart';
import 'package:bluestack/models/BluestackResponse.dart';
import 'package:bluestack/models/Tournaments.dart';
import 'package:bluestack/screens/LoginScreen.dart';
import 'package:bluestack/utils/Util.dart';
import 'package:flutter/cupertino.dart';

class HomeScreenController {
  List<Tournaments> _tournamentsList = List();
  bool _loading = false;
  bool _error = false;
  bool _stopServiceCall = false;
  String _cursor;
  Observer observer;
  BuildContext context;

  HomeScreenController(this.observer,this.context);

  List<Tournaments> getTournamentList() {
    return _tournamentsList;
  }

  void logout(){
    GatewayInteractor().setMobileNumber('');
    Util.movePage(context, (){
      return LoginScreen();
    },replacement: true);
  }

  void getGames() async {
    _showHideLoading(true);
    GatewayInteractor().getGames(_cursor).then((value) {
      if (value == null) {
        _showHideError(true);
        return;
      }
      _showHideError(false);

      BluestackResponse bluestackResponse =
      BluestackResponse.fromJson(jsonDecode(value.body));
      _cursor = bluestackResponse.data.cursor;
      _stopServiceCall = _cursor == null || _cursor.length <= 0;
      _tournamentsList.addAll(bluestackResponse.data.tournaments);
      _showHideLoading(false);
    });

  }

  bool isLoading() {
    return _loading;
  }

  bool isError() {
    return _error;
  }

  bool _isLoadToMore(int index) => (index == _tournamentsList.length &&
      index > 0 &&
      !_loading &&
      !_stopServiceCall);

  void loadMoreIfNeeded(int index) {
    if (!_isLoadToMore(index)) return;
    Future.delayed(Duration(milliseconds: 200), () {
      getGames();
    });
  }

  void _showHideError(bool showError) {
    _error = showError;
    observer.notify();
  }

  void _showHideLoading(bool load) {
    _loading = load;
    observer.notify();
  }
}
