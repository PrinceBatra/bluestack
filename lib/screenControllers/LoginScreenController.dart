import 'package:bluestack/repository/GatewayInteractor.dart';
import 'package:bluestack/screens/HomeScreen.dart';
import 'package:bluestack/utils/Util.dart';
import 'package:flutter/cupertino.dart';

class LoginScreenController {
  BuildContext context;

  LoginScreenController(this.context);

  void loginClicked(String phoneNumber, String password) {
    GatewayInteractor().setMobileNumber(phoneNumber).then((value) {
      Util.movePage(context, () {
        return HomeScreen();
      }, replacement: true);
    });
  }
}
